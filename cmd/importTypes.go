// Copyright © 2019 Kori <kori.irrlicht.dev@gmail.com>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package cmd

import (
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// importTypesCmd represents the importTypes command
var importTypesCmd = &cobra.Command{
	Use:   "importTypes",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		f, err := ioutil.ReadFile("notes.org")
		if err != nil {
			logrus.WithError(err).Fatalln("could not read file")
		}

		index := strings.Index(string(f), "** Type Matchup")
		textSplit := strings.Split(string(f[index:]), "\n")[1:]

		table := make([][]string, 0)

		for _, text := range textSplit {
			text = strings.ReplaceAll(text, " ", "")
			text = strings.ToLower(text)
			n := len(text)
			if n < 1 {
				continue
			}
			textAr := strings.Split(text[1:n-1], "|")
			for i, t := range textAr {
				if t == "" {
					textAr[i] = "1"
				}
			}
			table = append(table, textAr)
		}

		head := table[0]
		body := table[2:]

		result := "insert into element (name) values \n"
		for _, h := range head[1:] {
			result += fmt.Sprintf("('%s'),\n", h)
		}
		result = result[:len(result)-2]
		result += "\n on conflict do nothing;\n"

		result += "insert into element_matchup (element_offensive, element_defensive, modifier) values \n"
		for i, def := range head[1:] {
			for _, atk := range body {
				result += fmt.Sprintf("('%s','%s', %s),\n", atk[0], def, atk[i+1])

			}
		}

		result = result[:len(result)-2]
		result += "\n on conflict (element_offensive, element_defensive) do update set modifier = excluded.modifier;"

		ioutil.WriteFile("sql/importTypes.sql", []byte(result), 0600)

	},
}

func init() {
	rootCmd.AddCommand(importTypesCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// importTypesCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// importTypesCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
