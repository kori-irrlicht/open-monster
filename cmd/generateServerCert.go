// Copyright © 2019 Kori <kori.irrlicht.dev@gmail.com>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package cmd

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"math/big"
	"net"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// generateServerCertCmd represents the generateServerCert command
var generateServerCertCmd = &cobra.Command{
	Use:   "generateServerCert",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		host := viper.GetString("server.cert.host")
		if len(host) == 0 {
			logrus.Fatalln("Missing --host parameter")
		}

		var priv interface{}
		var err error
		switch viper.GetString("server.cert.ecdsa-curve") {
		case "":
			priv, err = rsa.GenerateKey(rand.Reader, viper.GetInt("server.cert.rsa-bits"))
		case "P224":
			priv, err = ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
		case "P256":
			priv, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
		case "P384":
			priv, err = ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
		case "P521":
			priv, err = ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
		default:
			logrus.WithField("curve", viper.GetString("server.cert.ecdsa-curve")).Fatalln("Unrecognized elliptic curve")
		}

		if err != nil {
			logrus.WithError(err).Fatalln("failed to generate private key")
		}

		var notBefore time.Time
		validFrom := viper.GetString("server.cert.start-date")
		if len(validFrom) == 0 {
			notBefore = time.Now()
		} else {
			notBefore, err = time.Parse("Jan 2 15:04:05 2006", validFrom)
			if err != nil {
				logrus.WithError(err).Fatalln("failed to parse creation date")
			}
		}

		notAfter := notBefore.Add(viper.GetDuration("server.cert.duration"))

		serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
		serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
		if err != nil {
			logrus.WithError(err).Fatalln("failed to generate serial number")
		}

		template := x509.Certificate{
			SerialNumber: serialNumber,
			Subject: pkix.Name{
				Organization: []string{viper.GetString("server.cert.organization")},
			},
			NotBefore:             notBefore,
			NotAfter:              notAfter,
			KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
			BasicConstraintsValid: true,
			IsCA:                  true,
		}

		hosts := strings.Split(host, ",")
		for _, h := range hosts {
			if ip := net.ParseIP(h); ip != nil {
				template.IPAddresses = append(template.IPAddresses, ip)
			} else {
				template.DNSNames = append(template.DNSNames, h)
			}
		}

		derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, publicKey(priv), priv)
		if err != nil {
			logrus.WithError(err).Fatalln("failed to create certificate")
		}

		outfile := viper.GetString("server.cert.outfile")
		certOut, err := os.OpenFile(outfile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
		if err != nil {
			logrus.WithError(err).WithField("file", outfile).Fatalln("failed to open file for writing")
		}
		if err := pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes}); err != nil {
			logrus.WithError(err).WithField("file", outfile).Fatalln("failed to write file")
		}
		if err := certOut.Close(); err != nil {
			logrus.WithError(err).WithField("file", outfile).Fatalln("failed to close file")
		}
		logrus.Infoln("Wrote cert")

		keyfile := viper.GetString("server.cert.keyfile")
		keyOut, err := os.OpenFile(keyfile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)

		if err != nil {
			logrus.WithError(err).WithField("file", keyfile).Fatalln("failed to open file for writing")
		}
		if err := pem.Encode(keyOut, pemBlockForKey(priv)); err != nil {
			logrus.WithError(err).WithField("file", keyfile).Fatalln("failed to write file")
		}
		if err := keyOut.Close(); err != nil {
			logrus.WithError(err).WithField("file", keyfile).Fatalln("failed to close file")
		}

		logrus.Infoln("Wrote key")
	},
}

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func pemBlockForKey(priv interface{}) *pem.Block {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)}
	case *ecdsa.PrivateKey:
		b, err := x509.MarshalECPrivateKey(k)
		if err != nil {
			logrus.WithError(err).Fatalln("unable to marshal ECDSA private key")
		}
		return &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}
	default:
		return nil
	}
}

func init() {
	rootCmd.AddCommand(generateServerCertCmd)

	generateServerCertCmd.PersistentFlags().String("host", "", "Comma-separated hostnames and IPs to generate a certificate for")
	generateServerCertCmd.PersistentFlags().String("organization", "", "Organization name to generate a certificate for")
	generateServerCertCmd.PersistentFlags().String("outfile", "cert.pem", "Write the certificate to this file")
	generateServerCertCmd.PersistentFlags().String("keyfile", "key.pem", "Write the key for the certificate to this file")
	generateServerCertCmd.PersistentFlags().String("start-date", "", "Creation date formatted as Jan 1 15:04:05 2011")
	generateServerCertCmd.PersistentFlags().Duration("duration", 365*24*time.Hour, "Duration that certificate is valid for")
	generateServerCertCmd.PersistentFlags().Int("rsa-bits", 2048, "Size of RSA key to generate. Ignored if --ecdsa-curve is set")
	generateServerCertCmd.PersistentFlags().String("ecdsa-curve", "", "ECDSA curve to use to generate a key. Valid values are P224, P256 (recommended), P384, P521")
	viper.BindPFlag("server.cert.host", generateServerCertCmd.PersistentFlags().Lookup("host"))
	viper.BindPFlag("server.cert.organization", generateServerCertCmd.PersistentFlags().Lookup("organization"))
	viper.BindPFlag("server.cert.outfile", generateServerCertCmd.PersistentFlags().Lookup("outfile"))
	viper.BindPFlag("server.cert.keyfile", generateServerCertCmd.PersistentFlags().Lookup("keyfile"))
	viper.BindPFlag("server.cert.start-date", generateServerCertCmd.PersistentFlags().Lookup("start-date"))
	viper.BindPFlag("server.cert.duration", generateServerCertCmd.PersistentFlags().Lookup("duration"))
	viper.BindPFlag("server.cert.rsa-bits", generateServerCertCmd.PersistentFlags().Lookup("rsa-bits"))
	viper.BindPFlag("server.cert.ecdsa-curve", generateServerCertCmd.PersistentFlags().Lookup("ecdsa-curve"))

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateServerCertCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateServerCertCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
