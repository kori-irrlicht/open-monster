
drop function insertNewUser(p_name text, p_x text, p_y text, p_keyname text, p_curve char(5));

drop function insertNewCharacter(p_name text, p_account_id int);

drop table evolution_condition;
drop table evolution_category;
drop table evolution;
drop table monster;

drop table element_matchup;
drop table element;

drop table item;

drop view credential;
drop table publickey;
drop table character;
drop table account;
