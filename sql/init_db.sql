
create table if not exists account (
       id int generated always as identity primary key,
       name text not null
);

create table if not exists publickey (
       account_id int references account(id) not null,
       name text not null,
       x text not null,
       y text not null,
       curve char(5) not null check(curve = ANY('{P-224, P-256, P-384, P-521}'::text[])),
       primary key(account_id, name)
);

create table if not exists item (
      id int generated always as identity primary key,
      name text not null
);

create table if not exists element (
       name text not null primary key
);

create table if not exists element_matchup (
       element_offensive text references element(name) not null,
       element_defensive text  references element(name) not null,
       modifier decimal(2,1) not null,
       primary key(element_offensive, element_defensive)
);

create table if not exists monster (
       id int generated always as identity primary key,
       name text,
       type1 text references element(name) not null,
       type2 text references element(name),
       hp int not null,
       atk int not null,
       def int not null,
       spatk int not null,
       spdef int not null,
       speed int not null,
       attribute_modifier int not null,
       attribute_modifier_type varchar(5) check(attribute_modifier_type = ANY('{hp, atk, def, spatk, spdef, speed}'::text[]))
);

create table if not exists evolution(
       id int generated always as identity primary key,
       monster_from_id int references monster(id),
       monster_to_id int references monster(id),
       unique(monster_from_id, monster_to_id)
);

create table if not exists evolution_category(
       name text primary key
);

create table if not exists evolution_condition (
       evolution_id int references evolution(id) not null,
       evolution_category text references evolution_category(name) not null,
       condition json not null,
       primary key(evolution_id, evolution_category)
);

create table if not exists character (
       id int generated always as identity primary key,
       account_id int references account(id) not null,
       name text not null
);

create or replace function insertNewUser(p_name text, p_x text, p_y text, p_keyname text, p_curve char(5)) returns int as $$
       declare
       userid int;
       begin
        insert into account (id, name) values (default, p_name) returning id into userid;
        insert into publickey (account_id, name, x, y, curve) values (userid, p_keyname, p_x, p_y,  p_curve);
        return userid;
       end;
$$ language plpgsql;

create or replace function insertNewCharacter(p_name text, p_account_id int) returns int as $$
       declare
       character_id int;
       begin
        insert into character (id, account_id, name) values (default, p_account_id, p_name) returning id into character_id;
        return character_id;
       end;
$$ language plpgsql;

create or replace view credential as
       select a.id as account_id, a.name as account_name, pk.name as key_name, pk.x as key_x, pk.y as key_y, pk.curve as key_curve from account a join publickey pk on pk.account_id = a.id;
