package internal

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"errors"
	"fmt"
	"math/big"
	"net"
	"time"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	grpc_logrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	"github.com/patrickmn/go-cache"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/status"

	"gitlab.com/kori-irrlicht/open-monster/internal/database"
	"gitlab.com/kori-irrlicht/open-monster/internal/database/postgresql"
	"gitlab.com/kori-irrlicht/open-monster/internal/model"
	pb "gitlab.com/kori-irrlicht/open-monster/pkg/protobuf"

	_ "github.com/lib/pq"
)

var (
	db      database.OMDB
	session *cache.Cache
)

type server struct{}

func (as *server) CreateNewAccount(context context.Context, ar *pb.NewAccountRequest) (*pb.NewAccountResponse, error) {

	logger := logrus.WithField("method", "CreateNewAccount")
	logger.Debugln("Creating new account")

	pk := ecdsa.PublicKey{}
	var x big.Int
	var y big.Int

	if err := x.UnmarshalText([]byte(ar.Key.X)); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid x")
	}

	if err := y.UnmarshalText([]byte(ar.Key.Y)); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid y")
	}

	pk.X = &x
	pk.Y = &y

	switch ar.Key.Curve {
	case pb.PublicKey_P224:
		pk.Curve = elliptic.P224()
	case pb.PublicKey_P256:
		pk.Curve = elliptic.P256()
	case pb.PublicKey_P384:
		pk.Curve = elliptic.P384()
	case pb.PublicKey_P521:
		pk.Curve = elliptic.P521()
	default:
		return nil, status.Errorf(codes.InvalidArgument, "invalid elliptic curve")
	}

	if ok := verifySignature(&pk, ar.Sig); !ok {
		return nil, status.Errorf(codes.InvalidArgument, "could not verify signature")
	}

	id, err := db.CreateNewUser(ar.Username, ar.Key.X, ar.Key.Y, ar.Key.Name, pk.Curve.Params().Name)

	if err != nil {
		switch err.(type) {
		case *database.PublicKeyExistsError:
			logger.WithError(err).Warnln("could not insert new user")
			return nil, status.Errorf(codes.AlreadyExists, "could not create user")
		default:
			logger.WithError(err).Errorln("could not insert new user")
			return nil, status.Errorf(codes.Internal, "could not create user")

		}
	}

	logger.Debugln("created new user")

	return &pb.NewAccountResponse{Userid: int64(id)}, nil
}

func (s *server) CreateNewCharacter(ctx context.Context, cpr *pb.CreateCharacterRequest) (*pb.CreateCharacterResponse, error) {
	logger := logrus.WithField("method", "CreateNewCharacter")
	logger.Debugln("Creating new character")

	id, err := db.CreateNewCharacter(model.AccountId(cpr.AccountId), cpr.CharacterName)
	if err != nil {
		logger.WithError(err).WithField("account", cpr.AccountId).Infoln("could not create new character")
		return nil, status.Errorf(codes.FailedPrecondition, "could not could not create new character")
	}

	logger.Debugln("created new character")
	return &pb.CreateCharacterResponse{
		CharacterId: int64(id),
	}, nil
}

func (s *server) GetCharacters(ctx context.Context, gbr *pb.GetCharactersRequest) (*pb.GetCharactersResponse, error) {
	logger := logrus.WithField("method", "GetCharacters").WithField("accountId", gbr.AccountId)
	logger.Debugln("getCharacters")

	characters, err := db.GetCharacters(model.AccountId(gbr.AccountId))
	if err != nil {
		logger.WithError(err).Infoln("could not get characters for account")
		return nil, status.Errorf(codes.FailedPrecondition, "could not get characters for account")
	}

	chars := model.ToProtobufCharacters(characters)

	return &pb.GetCharactersResponse{
		Characters: chars,
	}, nil
}

func (s *server) LoadCharacter(ctx context.Context, lbr *pb.LoadCharacterRequest) (*pb.LoadCharacterResponse, error) {
	logger := logrus.WithFields(logrus.Fields{
		"method":      "GetCharacters",
		"accountId":   lbr.AccountId,
		"characterId": lbr.CharacterId,
	})

	accId := model.AccountId(lbr.AccountId)
	char, err := db.GetCharacter(accId, model.CharacterId(lbr.CharacterId))
	if err != nil {
		logger.WithError(err).Info("could not load character")
		return nil, status.Errorf(codes.NotFound, "could not find character")
	}

	sess := createSession(accId)
	sess.Character = char

	return &pb.LoadCharacterResponse{
		Character: char.ToProtobuf(),
	}, nil
}

func StartGrpcServer() {
	setupLogrus()
	setupSession()

	logrus.Infoln("Starting grpc server")

	var err error
	db, err = connectToDatabase()
	if err != nil {
		logrus.WithError(err).Fatalln("could not connect to database")
	}

	lis, err := net.Listen("tcp", ":"+viper.GetString("server.port"))
	if err != nil {
		logrus.WithError(err).Fatalln("could not create listener")
	}

	certFile := viper.GetString("server.cert.outfile")
	keyFile := viper.GetString("server.cert.keyfile")

	creds, err := credentials.NewServerTLSFromFile(certFile, keyFile)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"certFile": certFile,
			"keyFile":  keyFile,
		}).Fatalln("Could not create credentials")
	}

	srv := grpc.NewServer(grpc.Creds(creds), grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
		grpc_auth.UnaryServerInterceptor(authFunc),
		grpc_logrus.UnaryServerInterceptor(logrus.NewEntry(logrus.StandardLogger())),
	)))

	s := &server{}
	pb.RegisterAccountServiceServer(srv, s)
	pb.RegisterCharacterServiceServer(srv, s)

	logrus.Infoln("Server started. Begin listening...")
	if err := srv.Serve(lis); err != nil {
		logrus.WithError(err).Fatalln("could not listen")
	}
}

func connectToDatabase() (database.OMDB, error) {

	dbName := viper.GetString("db.name")
	dbUser := viper.GetString("db.user")
	dbPassword := viper.GetString("db.password")
	dbPort := viper.GetString("db.port")
	dbHost := viper.GetString("db.host")

	logrus.WithFields(logrus.Fields{
		"user": dbUser,
		"db":   dbName,
		"host": dbHost,
		"port": dbPort,
	}).Infoln("Connecting to database")

	return postgresql.NewDB(dbUser, dbPassword, dbName, dbHost, dbPort)
}

func setupLogrus() {
	switch viper.GetString("server.logger.level") {
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
	case "info":
		logrus.SetLevel(logrus.InfoLevel)
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
	default:
		logrus.SetLevel(logrus.InfoLevel)
	}
	customFormatter := new(logrus.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05 -07:00"
	customFormatter.FullTimestamp = true
	logrus.SetFormatter(customFormatter)
}

func setupSession() {
	timeout := time.Second * viper.GetDuration("session.timeout")
	clearInterval := time.Second * viper.GetDuration("session.clearInterval")

	session = cache.New(timeout, clearInterval)
	session.OnEvicted(func(key string, value interface{}) {
		logrus.WithFields(logrus.Fields{
			"key":   key,
			"value": value,
		}).Debugln("evicted entry")
	})

}

func createSession(accountId model.AccountId) (s *Session) {
	key := fmt.Sprintf("%d", accountId)
	s = &Session{}
	session.Set(key, s, cache.DefaultExpiration)
	return s
}

func getSession(accountId model.AccountId) (s *Session, err error) {
	key := fmt.Sprintf("%d", accountId)
	sess, ok := session.Get(key)
	if !ok {
		return nil, errors.New("session does not exist")
	}
	s = sess.(*Session)
	session.Set(key, s, cache.DefaultExpiration)
	return s, nil
}

type Session struct {
	Character *model.Character
	Test      string
}
