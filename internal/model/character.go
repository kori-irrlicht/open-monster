package model

import pb "gitlab.com/kori-irrlicht/open-monster/pkg/protobuf"

type CharacterId int64

type Character struct {
	Id   CharacterId
	Name string
}

func (c *Character) ToProtobuf() *pb.Character {
	return &pb.Character{
		Id:   int64(c.Id),
		Name: c.Name,
	}
}

func ToProtobufCharacters(chars []*Character) []*pb.Character {
	res := make([]*pb.Character, 0)
	for _, char := range chars {
		c := char.ToProtobuf()
		res = append(res, c)
	}
	return res

}
