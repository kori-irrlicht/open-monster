package model

import "crypto/ecdsa"

type Publickey struct {
	Key  *ecdsa.PublicKey
	Name string
}
