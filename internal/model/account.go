package model

import "crypto/ecdsa"

type AccountId int64

type Account struct {
	ID   AccountId
	Name string
	Keys map[string]*ecdsa.PublicKey
}
