package postgresql

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"database/sql"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"

	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/open-monster/internal/database"
	"gitlab.com/kori-irrlicht/open-monster/internal/model"
)

type postgresDB struct {
	db *sql.DB
}

func (p *postgresDB) CreateNewUser(username, x, y, keyname, curve string) (model.AccountId, error) {

	exists := false
	p.db.QueryRow("select true from publickey pk where pk.x = $1 and pk.y = $2", x, y).Scan(&exists)
	if exists {
		return 0, &database.PublicKeyExistsError{
			X: x,
			Y: y,
		}
	}

	var id model.AccountId = 0
	err := p.db.QueryRow("select insertNewUser($1,$2,$3,$4,$5)", username, x, y, keyname, curve).Scan(&id)
	if err != nil {
		return 0, logError(err, "could not insert new user")
	}
	return id, nil
}

func (p *postgresDB) CreateNewCharacter(user model.AccountId, name string) (model.CharacterId, error) {
	exists := false
	p.db.QueryRow("select true from account a where a.id = $1", user).Scan(&exists)
	if !exists {
		return 0, errors.New("account does not exist")
	}

	var id model.CharacterId = 0
	err := p.db.QueryRow("select insertNewCharacter($1,$2)", name, user).Scan(&id)
	if err != nil {
		return 0, logError(err, "could not insert new character")
	}
	return id, nil

}

const selectCharacters = `
select
  id
, name
from character
where id = $1
`

func (p *postgresDB) GetCharacters(accountId model.AccountId) (characters []*model.Character, err error) {

	rows, err := p.db.Query(selectCharacters, accountId)
	if err != nil {
		return nil, logError(err, "could not execute query")
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id   model.CharacterId
			name string
		)

		if err := rows.Scan(&id, &name); err != nil {
			return nil, logError(err, "could not scan row")
		}

		char := &model.Character{
			Id:   id,
			Name: name,
		}
		characters = append(characters, char)
	}
	if err := rows.Err(); err != nil {
		return nil, logError(err, "expected more rows")
	}
	return characters, err

}

const selectCharacter = `
select
  c.id
, c.name
from character c
where c.account_id = $1
and c.id = $2
`

func (p *postgresDB) GetCharacter(accountId model.AccountId, characterId model.CharacterId) (*model.Character, error) {

	var (
		id   model.CharacterId
		name string
	)
	if err := p.db.QueryRow(selectCharacter, accountId, characterId).Scan(&id, &name); err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("empty result")
		}
		logrus.WithError(err).Errorln("could not execute query")
		return nil, errors.New("could not execute query")
	}

	return &model.Character{
		Id:   id,
		Name: name,
	}, nil
}

const selectAccount = `
select
  account_id
, account_name
, key_name
, key_x
, key_y
, key_curve
from credential
where account_id = $1
`

func (p *postgresDB) Account(accountId model.AccountId) (acc *model.Account, err error) {
	logrus.WithField("accountId", accountId).Debugln("get account")

	rows, err := p.db.Query(selectAccount, accountId)
	if err != nil {
		return nil, logError(err, "could not select account")
	}
	defer rows.Close()
	acc = &model.Account{
		Keys: make(map[string]*ecdsa.PublicKey),
	}
	for rows.Next() {
		var (
			id      model.AccountId
			name    string
			keyName string
			x       string
			y       string
			curve   string
		)
		if err := rows.Scan(&id, &name, &keyName, &x, &y, &curve); err != nil {
			return nil, logError(err, "could not scan row")
		}
		acc.ID = id
		acc.Name = name

		elCurve, err := mapCurve(curve)
		if err != nil {
			return nil, logErrorWithFields(err, "could not map elliptic curve", logrus.Fields{"curve": curve})
		}

		pk := &ecdsa.PublicKey{}

		pk.Curve = elCurve

		var (
			xUn big.Int
			yUn big.Int
		)

		xUn.UnmarshalText([]byte(x))
		yUn.UnmarshalText([]byte(y))

		pk.X = &xUn
		pk.Y = &yUn

		acc.Keys[keyName] = pk
	}

	if err := rows.Err(); err != nil {
		return nil, logError(err, "expected more rows")
	}

	if len(acc.Keys) == 0 {
		return nil, errors.New("resultset was empty")
	}

	return acc, err
}

func mapCurve(curve string) (elliptic.Curve, error) {
	switch curve {
	case "P-224":
		return elliptic.P224(), nil
	case "P-256":
		return elliptic.P256(), nil
	case "P-384":
		return elliptic.P384(), nil
	case "P-521":
		return elliptic.P521(), nil
	}
	return nil, errors.New("unknown curve")
}

func NewDB(dbUser, dbPassword, dbName, dbHost, dbPort string) (database.OMDB, error) {

	p := postgresDB{}

	logrus.WithFields(logrus.Fields{
		"user": dbUser,
		"db":   dbName,
		"host": dbHost,
		"port": dbPort,
	}).Infoln("Connecting to database")

	connStr := fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s sslmode=disable", dbUser, dbPassword, dbHost, dbPort, dbName)
	db, err := sql.Open("postgres", connStr)
	p.db = db
	if err != nil {
		return nil, logError(err, "could not connect to database")
	}

	if err := initDatabase(p.db); err != nil {
		return nil, logError(err, "could not initialize database")
	}

	return &p, nil

}

func initDatabase(db *sql.DB) error {
	logrus.Infoln("Initialize database")
	file := "./sql/init_db.sql"
	initDb, err := ioutil.ReadFile(file)
	if err != nil {
		return logErrorWithFields(err, "failed to load file", logrus.Fields{"file": file})
	}
	if _, err = db.Exec(string(initDb)); err != nil {
		return logError(err, "failed to execute statement")
	}
	logrus.Infoln("Initialize database: finished")
	return nil
}

func logError(err error, msg string) error {
	return logErrorWithFields(err, msg, logrus.Fields{})
}

func logErrorWithFields(err error, msg string, fields logrus.Fields) error {
	logrus.WithError(err).WithFields(fields).Errorln(msg)
	return errors.New(msg)
}
