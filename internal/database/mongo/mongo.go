package mongo

import (
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/open-monster/internal/database"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type mongoDb struct {
	db *mgo.Database
}

type user struct {
	Id         bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name       string
	Publickeys []*publickey
}

type publickey struct {
	X    string
	Y    string
	Name string
}

func (m *mongoDb) CreateNewUser(name, x, y, keyname string) (string, error) {

	id := bson.NewObjectId()
	c := m.db.C("user")
	err := c.Insert(&user{
		Id:   id,
		Name: name,
		Publickeys: []*publickey{
			&publickey{
				X:    x,
				Y:    y,
				Name: keyname,
			},
		},
	})

	if err != nil {
		logrus.WithError(err).Errorln("could not insert user")
		return "", errors.New("could not insert user")
	}

	return id.Hex(), nil
}

func NewDB(dbUser, dbPassword, dbName, dbHost, dbPort string) (database.OMDB, error) {

	logrus.WithFields(logrus.Fields{
		"user": dbUser,
		"db":   dbName,
		"host": dbHost,
		"port": dbPort,
	}).Infoln("Connecting to database")
	db := &mongoDb{}

	session, err := mgo.Dial(fmt.Sprintf("mongodb://%s:%s@%s:%s", dbUser, dbPassword, dbHost, dbPort))

	if err != nil {
		logrus.WithError(err).Errorln("could not connect to mongoDB")
		return nil, errors.New("could not connect to mongoDB")
	}

	db.db = session.DB(dbName)

	logrus.Infoln("connected to database")

	return db, nil
}
