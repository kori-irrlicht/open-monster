package database

import (
	"fmt"

	"gitlab.com/kori-irrlicht/open-monster/internal/model"
)

type OMDB interface {
	CreateNewUser(name, x, y, keyname, curve string) (model.AccountId, error)
	CreateNewCharacter(user model.AccountId, name string) (model.CharacterId, error)
	Account(accountId model.AccountId) (*model.Account, error)
	GetCharacters(accountId model.AccountId) ([]*model.Character, error)
	GetCharacter(accountId model.AccountId, characterId model.CharacterId) (*model.Character, error)
}

type PublicKeyExistsError struct {
	X string
	Y string
}

func (e *PublicKeyExistsError) Error() string {
	return fmt.Sprintf("publickey already exists -> x: %s, y: %s", e.X, e.Y)
}
