package internal

import (
	"context"
	"crypto/ecdsa"
	"math/big"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/open-monster/internal/model"
	pb "gitlab.com/kori-irrlicht/open-monster/pkg/protobuf"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const (
	authTimeOut = time.Second * 5
)

func (s *server) AuthFuncOverride(ctx context.Context, fullMethodName string) (context.Context, error) {
	if strings.Index(fullMethodName, "CreateNewAccount") >= 0 {
		return ctx, nil
	}

	return authFunc(ctx)
}

func authFunc(ctx context.Context) (context.Context, error) {
	md, ok := metadata.FromIncomingContext(ctx)

	if !ok {
		return ctx, status.Errorf(codes.Internal, "could not parse metadata")
	}

	keyname := md.Get("signature_name")

	if len(keyname) == 0 {
		return ctx, status.Errorf(codes.InvalidArgument, "invalid keyname")
	}

	hash := md.Get("signature_hash")
	if len(hash) == 0 {
		return ctx, status.Errorf(codes.InvalidArgument, "invalid hash")
	}
	r := md.Get("signature_r")
	if len(r) == 0 {
		return ctx, status.Errorf(codes.InvalidArgument, "invalid r")
	}
	s := md.Get("signature_s")
	if len(s) == 0 {
		return ctx, status.Errorf(codes.InvalidArgument, "invalid s")
	}

	sig := &pb.Signature{
		Keyname: keyname[0],
		Hash:    hash[0],
		R:       r[0],
		S:       s[0],
	}

	t, err := time.Parse(time.RFC3339, sig.Hash)
	if err != nil {
		return ctx, status.Errorf(codes.InvalidArgument, "invalid timestamp")
	}

	dur := time.Since(t)
	if -authTimeOut >= dur || dur >= authTimeOut {
		return ctx, status.Errorf(codes.Unauthenticated, "authentication expired")
	}

	id, err := strconv.Atoi(md.Get("accountId")[0])
	if err != nil {
		return ctx, status.Errorf(codes.InvalidArgument, "invalid accountId")
	}

	return ctx, hasAccess(model.AccountId(id), sig)

}

func hasAccess(accountId model.AccountId, signature *pb.Signature) error {
	acc, err := account(accountId)
	if err != nil {
		return err
	}
	if err := checkSignature(acc, signature); err != nil {
		return err
	}
	return nil

}

func checkSignature(acc *model.Account, signature *pb.Signature) error {
	key, ok := acc.Keys[signature.Keyname]
	if !ok {
		logrus.WithField("name", signature.Keyname).Debugln("Unknown keyname")
		return status.Errorf(codes.Unauthenticated, "must be logged in")
	}

	if ok := verifySignature(key, signature); !ok {
		logrus.WithField("name", signature.Keyname).Debugln("could not verify signature")
		return status.Errorf(codes.Unauthenticated, "must be logged in")
	}
	return nil
}

func verifySignature(pk *ecdsa.PublicKey, sig *pb.Signature) bool {

	var r big.Int
	if err := r.UnmarshalText([]byte(sig.R)); err != nil {
		return false
	}

	var s big.Int
	if err := s.UnmarshalText([]byte(sig.S)); err != nil {
		return false
	}

	return ecdsa.Verify(pk, []byte(sig.Hash), &r, &s)
}

func account(accountId model.AccountId) (*model.Account, error) {
	acc, err := db.Account(accountId)
	if err != nil {
		logrus.WithError(err).WithField("account", accountId).Infoln("could not find account")
		return nil, status.Errorf(codes.FailedPrecondition, "could not find account")
	}
	return acc, err
}
